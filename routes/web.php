<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

Route::group(['prefix' => 'api', 'middleware' => 'cors'], function ($router) {
    Route::group(['middleware' => 'auth'], function ($router) {
        $router->get('/', 'WelcomeController@index');
        $router->get('/users', 'UserController@index');
        $router->get('/users/{id}', 'UserController@show');
        $router->post('/create', 'UserController@create');
        $router->put('/users/{id}', 'UserController@update');
        $router->delete('/users/{id}', 'UserController@delete');
    });

    $router->post('/auth/login', 'AuthController@login');
    $router->post('/auth/refresh_token', function () {
        try {
            $guard = Auth::guard();
            return response()->json(['token' => $guard->refresh()]);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $exception) {
            return response()->json(['error' => 'Cound not refresh token.'], 400);
        }
    });
});