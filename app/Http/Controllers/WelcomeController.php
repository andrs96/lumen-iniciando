<?php

namespace App\Http\Controllers;

class WelcomeController extends Controller
{
    public function index()
    {
        $name = 'André';
        return view('welcome', ['n'=>$name]);
    }
}